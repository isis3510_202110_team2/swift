//
//  ProfileView.swift
//  togo
//
//  Created by Andrea Montoya on 2/04/21.
//

import SwiftUI
import Firebase

struct ProfileView: View {
    
    @Binding var autenticated: Bool
    @ObservedObject var user = UserViewModel()
    
    var body: some View {
        ZStack {
            LightSlateGrey.ignoresSafeArea()
            VStack {
                Image("susan").resizable()
                    .frame(width: 120, height: 120, alignment: .center)
                Divider()
                
                HStack{
                    VStack{
                        Text("Name: \(user.user.name)")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color.black)
                            .frame(width: 200, alignment: .leading)
                        Text("Telephone: \(user.user.phoneNumber)")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color.black)
                            .frame(width: 200, alignment: .leading)
                    }
                    Button(action: {}) {
                        Image("edit").resizable()
                            .frame(width: 20, height: 20, alignment: .trailing)
                    }
                }
                Divider()
                
                HStack {
                    Image("cat1").resizable()
                        .frame(width: 50, height: 50, alignment: .center)
                    Button(action: {}) {
                        Text("Michi")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .cornerRadius(5.0)
                    }
                }
                
                HStack {
                    Image("").resizable()
                        .frame(width: 50, height: 50, alignment: .center)
                    Button(action: {}) {
                        Text("")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .cornerRadius(5.0)
                    }
                }
                HStack {
                    Image("x").resizable()
                        .frame(width: 50, height: 50, alignment: .center)
                    Button(action: {}) {
                        Text("")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .cornerRadius(5.0)
                    }
                }
                
                VStack {
                    Button(action: {}) {
                        Image("plus").resizable()
                            .frame(width: 40, height: 40, alignment: .center)
                    }
                    HStack{
                        
                        Image("x").resizable()
                            .frame(width: 150, height: 40, alignment: .center)
                        
                        Button(action: {
                            let this = Auth.auth().currentUser!.uid
                            print(this, "HERE")
                            let firebaseAuth = Auth.auth()
                            do {
                                try firebaseAuth.signOut()
                                autenticated = false
                            }catch let signOutError as NSError {
                                print(signOutError, "ERROR AT SIGNING OUT")
                            }
                        }) {
                            Text("Log out")
                                .padding()
                                .foregroundColor(Color.black)
                                .frame(width: 100.0)
                                .background(LightSlateGrey)
                                .cornerRadius(5.0)
                            Image("log-out").resizable()
                                .frame(width: 25, height: 25, alignment: .center)
                        }
                    }
                }
            }
            .padding(.bottom, 50.0)
            .frame(width: 300.0,height: 570.0 )
        }
        .navigationTitle("Welcome \(user.user.name)")
    }
}

//struct ProfileView_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileView()
//            .previewDevice("iPod touch (7th generation)")
//    }
//}
