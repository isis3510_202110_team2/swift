//
//  LogIn.swift
//  togo
//
//  Created by David Manosalva on 1/04/21.
//

import SwiftUI
import FirebaseAuth

struct LogIn: View {
    @State var email = ""
    @State var password = ""
    
    var body: some View {
        ScrollView{
            TextField("Email", text: $email)
                .padding()
                .keyboardType(UIKeyboardType.emailAddress)
                .disableAutocorrection(true)
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
            
            SecureField("Password", text: $password)
                .padding()
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
                .disableAutocorrection(true)
            
            Button(action:{
                Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
                    guard let strongSelf = self else {return}
                    error ?? print("error", error!.localizedDescription)
                }
            }){
                Text("Send").padding()
                    .background(QueenBlue)
                    .foregroundColor(.white)
                    .cornerRadius(5)
            }
            
            Spacer()
        }.navigationTitle("Log In")
    }
}

struct LogIn_Previews: PreviewProvider {
    static var previews: some View {
        LogIn()
    }
}
