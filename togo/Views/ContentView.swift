//
//  ContentView.swift
//  togo
//
//  Created by David Manosalva on 29/03/21.
//

import SwiftUI
import FirebaseAuth
import FirebaseFirestore

struct ContentView: View {
    
    @State private var autenticated:Bool = false
    
    init() {
        autenticated = Auth.auth().currentUser != nil
        if autenticated {
            print(Auth.auth().currentUser!.uid)
        }
    }
    
    var body: some View {
        if !autenticated {
            LogOutView(autenticated:$autenticated)
        }
        else {
            HomeView(autenticated: $autenticated)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
                .previewDevice("iPhone 12 Pro Max")
            ContentView()
                .previewDevice("iPod touch (7th generation)")
        }
    }
}


