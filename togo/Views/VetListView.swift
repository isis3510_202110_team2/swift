//
//  VetListView.swift
//  togo
//
//  Created by Andrea Montoya on 1/04/21.
//

import Foundation
import SwiftUI

struct VetListView: View {
    var body: some View {
        ZStack {
            VStack {
                HStack {
                    Image("Vet1").resizable()
                        .frame(width: 70, height: 70, alignment: .center)
                    NavigationLink(destination:VetDetailView()) {
                        Text("Veterinary Emergency Group")
                            .padding()
                            .frame(width: 200.0, alignment: .leading)
                            .cornerRadius(5.0)
                    }
                }
                
                
                HStack {
                    Image("pawprint").resizable()
                        .frame(width: 70, height: 70, alignment: .center)
                    Button(action: {}) {
                        Text("Paws Veterinarian")
                            .padding()
                            .frame(width: 200.0, alignment: .leading)
                            .cornerRadius(5.0)
                    }
                }
                
                
                HStack {
                    Image("Vet2").resizable()
                        .frame(width: 70, height: 70, alignment: .center)
                    Button(action: {}) {
                        Text("Forever Vets")
                            .padding()
                            .frame(width: 200.0, alignment: .leading)
                            .cornerRadius(5.0)
                    }
                }
                
                HStack {
                    Image("Vet3").resizable()
                        .frame(width: 70, height: 70, alignment: .center)
                    Button(action: {}) {
                        Text("Doral Centre Animal Hospital")
                            .padding()
                            .frame(width: 200.0, alignment: .leading)
                            .cornerRadius(5.0)
                    }
                }
                
                
                HStack {
                    Image("Vet4").resizable()
                        .frame(width: 70, height: 70, alignment: .center)
                    Button(action: {}) {
                        Text("Briarwood Animal hospital")
                            .padding()
                            .frame(width: 200.0, alignment: .leading)
                            .cornerRadius(5.0)
                    }
                }
                
                
                HStack {
                    Image("Vet5").resizable()
                        .frame(width: 70, height: 70, alignment: .center)
                    Button(action: {}) {
                        Text("Westshore Veterinary Clinic")
                            .padding()
                            .frame(width: 200.0, alignment: .leading)
                            .cornerRadius(5.0)
                    }
                }
                
                
                HStack {
                    Image("Vet6").resizable()
                        .frame(width: 70, height: 70, alignment: .center)
                    Button(action: {}) {
                        Text("Paws and Hooves Veterinary")
                            .padding()
                            .frame(width: 200.0, alignment: .leading)
                            .cornerRadius(5.0)
                    }
                }
                
                
                
            }
            .padding(.bottom, 20.0)
        }
        .navigationTitle("Vets")
    }
}

struct VetListView_Previews: PreviewProvider {
    static var previews: some View {
        VetListView()
            .previewDevice("iPod touch (7th generation)")
    }
}
