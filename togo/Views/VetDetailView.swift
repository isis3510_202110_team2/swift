//
//  VetDetailView.swift
//  togo
//
//  Created by Andrea Montoya on 1/04/21.
//

import Foundation
import SwiftUI

struct VetDetailView: View {
    var body: some View {
        ZStack {
            VStack {
                Image("Vet1").resizable()
                    .frame(width: 120, height: 120, alignment: .center)
               
                HStack{
                    VStack{
                        
                        Text("Office Hours:")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 200, alignment: .leading)
                        Text("Monday to Friday 7am - 7pm")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 200, alignment: .leading)
                        Text("Saturday 8:30am - 12pm")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 200, alignment: .leading)
                    }
                    
                    
                    Button(action: {}) {
                        Image("chat-box").resizable()
                            .frame(width: 50, height: 50, alignment: .trailing)
                    }
                    
                }
              
                Divider()
                
                HStack{
                    VStack{
                        
                        Text("Contact Us: (954)538-9938")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 220, alignment: .leading)
                    }
                        Image("phonecall").resizable()
                            .frame(width: 30, height: 30, alignment: .center)
                }
                
                Divider()
               
                HStack{
                    Button(action: {}) {
                        Text("DOWNLOAD CATALOG")
                            .font(.system(size:12))
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 220, alignment: .center)
                            .background(QueenBlue)
                            .cornerRadius(8.0)
                    }
                    
                    Image("pdf").resizable()
                        .frame(width: 30, height: 30)
                }
                
                GoogleMapsView()
                    .frame(width: 320, height: 175)
            
            }
            .frame(width: 320.0,height: 560.0 )
        }.navigationTitle("Veterinary Emergency Group")
    }
}

//struct StoreDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        StoreDetailView()
//            .previewDevice("iPod touch (7th generation)")
//    }
//}

