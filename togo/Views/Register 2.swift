//
//  Register.swift
//  togo
//
//  Created by David Manosalva on 31/03/21.
//

import SwiftUI
import FirebaseAuth

struct Register: View {
    @State var email = ""
    @State var password = ""
    @State var name = ""
    @State var phone = ""
    
    var body: some View {
        ScrollView{
            TextField("Email", text: $email)
                .padding()
                .keyboardType(UIKeyboardType.emailAddress)
                .disableAutocorrection(true)
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
            
            TextField("Name", text: $name)
                .padding()
                .keyboardType(UIKeyboardType.namePhonePad)
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
            
            TextField("Phone", text: $phone)
                .padding()
                .keyboardType(UIKeyboardType.phonePad)
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
            
            SecureField("Password", text: $password)
                .padding()
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
                .disableAutocorrection(true)
            
            Button(action:{
                Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                    guard let user = authResult?.user, error == nil
                    else {
                        print("error", error!.localizedDescription)
                        return
                        // [END_EXCLUDE]
                    }
                    print("\(String(describing: user.email)) created")
                }
            }){
                Text("Send").padding()
                    .background(QueenBlue)
                    .foregroundColor(.white)
                    .cornerRadius(5)
            }
            
            Spacer()
        }.navigationTitle("Register")
    }
}

struct Register_Previews: PreviewProvider {
    static var previews: some View {
        Register()
    }
}
