//
//  LogOutView.swift
//  togo
//
//  Created by David Manosalva on 31/03/21.
//

import SwiftUI

struct LogOutView: View {
    
    @Binding var autenticated: Bool
    
    var body: some View {
        NavigationView {
            ZStack {
                LightSlateGrey.ignoresSafeArea()
                VStack {
                    Spacer()
                    Image("husky-siberiano").resizable()
                        .frame(width: 300, height: 300, alignment: .center)
                    Text("Togo")
                        .font(.largeTitle)
                        .fontWeight(.semibold)
                        .padding()
                        .foregroundColor(.white)
                    Spacer()
                    HStack {
                        Spacer()
                        NavigationLink(destination: Register(autenticated: $autenticated)){Text("Register")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .background(QueenBlue)
                            .cornerRadius(5.0)}
                        Spacer()
                        NavigationLink(destination: LogIn(autenticated: $autenticated)){Text("Login")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .background(WildBlueYonder)
                            .cornerRadius(5.0)}
                        Spacer()
                    }
                }
                .padding(.bottom, 15.0)
            }
        }
    }
}

//struct LogOutView_Previews: PreviewProvider {
//    static var previews: some View {
//        LogOutView()
//    }
//}
