//
//  LogIn.swift
//  togo
//
//  Created by David Manosalva on 1/04/21.
//

import SwiftUI
import FirebaseAuth

struct LogIn: View {
    @State var email = ""
    @State var password = ""
    
    @State private var showAlert = false
    @State var errorMsg = ""
    
    @Binding var autenticated: Bool
    
    var body: some View {
        ScrollView{
            TextField("Email", text: $email)
                .padding()
                .keyboardType(UIKeyboardType.emailAddress)
                .disableAutocorrection(true)
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
            
            SecureField("Password", text: $password)
                .padding()
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
                .disableAutocorrection(true)
            
            Button(action:{
                Auth.auth().signIn(withEmail: email, password: password) {authResult, error in
                    guard let user = authResult?.user, error == nil else
                    {
                        showAlert = true
                        errorMsg = error!.localizedDescription
                        return
                    }
                    autenticated = true
                    print("\(String(describing: user.email)) logged in" )
                }
                
            }){
                Text("Send").padding()
                    .background(QueenBlue)
                    .foregroundColor(.white)
                    .cornerRadius(5)
            }.alert(isPresented: $showAlert){
                Alert(title:Text("Error"), message: Text("\(errorMsg)"), dismissButton: .default(Text("Ok"))) }
            
            Spacer()
        }.navigationTitle("Log In")
    }
}
//
//struct LogIn_Previews: PreviewProvider {
//    static var previews: some View {
//        LogIn()
//    }
//}
