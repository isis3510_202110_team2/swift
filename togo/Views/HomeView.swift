//
//  HomeView.swift
//  togo
//
//  Created by Andrea Montoya on 31/03/21.
//

import SwiftUI
import Firebase
import FirebaseAuth

struct HomeView: View {
    
    @Binding var autenticated: Bool
    @ObservedObject var displayName = UserNameViewModel()
    
    var body: some View {
        NavigationView{
            ZStack {
                LightSlateGrey.ignoresSafeArea()
                VStack {
                    
                    Text("Welcome, \(displayName.userName)!")
                        .font(.system(size:30))
                        .fontWeight(.medium)
                        .padding()
                        .foregroundColor(.black)
                    
                    
                    HStack {
                        Image("pet").resizable()
                            .frame(width: 80, height: 80, alignment: .center)
                        Button(action: {}) {
                            Text("Find My Pet")
                                .padding()
                                .foregroundColor(Color.white)
                                .frame(width: 150.0)
                                .background(WildBlueYonder)
                                .cornerRadius(5.0)
                        }
                    }
                    HStack {
                        Image("dog-house").resizable()
                            .frame(width: 80, height: 80, alignment: .center)
                        NavigationLink(destination: ServicesView()) {
                            Text("Services")
                                .padding()
                                .foregroundColor(Color.white)
                                .frame(width: 150.0)
                                .background(WildBlueYonder)
                                .cornerRadius(5.0)
                        }
                    }
                    HStack {
                        Image("chat-box").resizable()
                            .frame(width: 80, height: 80, alignment: .center)
                        Button(action: {}) {
                            Text("Chat")
                                .padding()
                                .foregroundColor(Color.white)
                                .frame(width: 150.0)
                                .background(WildBlueYonder)
                                .cornerRadius(5.0)
                        }
                    }
                    
                    HStack {
                        Image("user").resizable()
                            .frame(width: 80, height: 80, alignment: .center)
                        NavigationLink(destination: ProfileView(autenticated: $autenticated)) {
                            Text("My profile")
                                .padding()
                                .foregroundColor(Color.white)
                                .frame(width: 150.0)
                                .background(WildBlueYonder)
                                .cornerRadius(5.0)
                        }
                    }
                    Button(action: {
                        let this = Auth.auth().currentUser!.uid
                        print(this, "HERE")
                        let firebaseAuth = Auth.auth()
                        do {
                            try firebaseAuth.signOut()
                            autenticated = false
                        }catch let signOutError as NSError {
                            print(signOutError, "ERROR AT SIGNING OUT")
                        }
                    }){
                        Text("Log out")
                            .padding()
                            .foregroundColor(Color.black)
                            .frame(width: 100.0)
                            .background(LightSlateGrey)
                            .cornerRadius(5.0)
                        Image("log-out").resizable()
                            .frame(width: 25, height: 25, alignment: .center)
                    }
                    
                }
                .padding(.bottom, 15.0)
            }
        }
    }
}

//struct HomeView_Previews: PreviewProvider {
//    static var previews: some View {
//        HomeView()
//            .previewDevice("iPod touch (7th generation)")
//    }
//}
