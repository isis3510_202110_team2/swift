//
//  Register.swift
//  togo
//
//  Created by David Manosalva on 31/03/21.
//

import SwiftUI
import Firebase
import FirebaseAuth

struct Register: View {
    @State private var email = ""
    @State private var password = ""
    @State private var name = ""
    @State private var phone = ""
    
    @State private var showAlert = false
    @State var errorMsg = ""
    @Binding var autenticated: Bool
    
    @State private var showImagePicker : Bool = false
    @State private var image : Image? = Image("user")
    
    var body: some View {
        ScrollView{
            
            image?
                        .resizable()
                        .frame(width: 200, height: 200, alignment: .center)
                        .clipShape(Circle())
            Button("Choose a profile photo"){
            self.showImagePicker = true
                         }.padding()
                          .background(QueenBlue)
                          .foregroundColor(Color.white)
                          .cornerRadius(10)
                
            
            TextField("Email", text: $email)
                .padding()
                .keyboardType(UIKeyboardType.emailAddress)
                .disableAutocorrection(true)
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
            
            TextField("Name", text: $name)
                .padding()
                .keyboardType(UIKeyboardType.namePhonePad)
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
            
            TextField("Phone", text: $phone)
                .padding()
                .keyboardType(UIKeyboardType.phonePad)
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
            
            SecureField("Password", text: $password)
                .padding()
                .overlay(RoundedRectangle(cornerRadius: 15).stroke(QueenBlue, lineWidth: 2))
                .padding(10)
                .disableAutocorrection(true)
            
            Button(action:{
                Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                    guard let user = authResult?.user, error == nil else
                    {
                        showAlert = true
                        errorMsg = error!.localizedDescription
                        return
                    }
                    let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                    changeRequest?.displayName = name
                    changeRequest?.commitChanges { (error) in
                        print("ERROR \(String(describing: error))")
                    }
                    let db = Firestore.firestore()
                    let backendUser:User = User(name: name, email: email, phone: phone)
                    let data = backendUser.toDictionary()
                    let json = try! JSONSerialization.jsonObject(with: data!, options: [])
                    let dict = json as! [String: Any]
                    db.collection("User").document(user.uid).setData(dict){
                        err in
                        if let err = err {
                            print("ERROR", err)
                        }
                    }
                    autenticated = true
                }
            })
            {
                Text("Send").padding()
                    .background(QueenBlue)
                    .foregroundColor(.white)
                    .cornerRadius(5)
            }.alert(isPresented: $showAlert){
                Alert(title:Text("Error"), message: Text("\(errorMsg)"), dismissButton: .default(Text("Ok"))) }
            
            Spacer()
        }.sheet(isPresented: self.$showImagePicker)
        {
           PhotoCaptureView(showImagePicker: self.$showImagePicker, image: self.$image)
         }.navigationTitle("Register")
         
    }
    
  
}

//struct Register_Previews: PreviewProvider {
//    static var previews: some View {
//        Register()
//    }
//}
