//
//  StoreDetailView.swift
//  togo
//
//  Created by Andrea Montoya on 1/04/21.
//

import Foundation
import SwiftUI

struct StoreDetailView: View {
    var body: some View {
        ZStack {
            VStack {
                    
                Button(action: {}) {
                    Image("left-arrow").resizable()
                        .frame(width: 15, height: 15)
                        .foregroundColor(Color.black)
                        .frame(width: 300, height: 50, alignment: .leading)
                        .cornerRadius(5.0)
                }
                    
                    Text("ExoticPet")
                        .font(.system(size:25))
                        .fontWeight(.medium)
                        .padding()
                        .foregroundColor(.black)
                        .frame(width: 300.0, alignment: .center)
                
                Divider()
 
                Image("snakes").resizable()
                    .frame(width: 120, height: 120, alignment: .center)
               
                HStack{
                    VStack{
                        
                        Text("Office Hours:")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 200, alignment: .leading)
                        Text("Monday to Friday 7am - 7pm")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 200, alignment: .leading)
                        Text("Saturday 8:30am - 12pm")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 200, alignment: .leading)
                    }
                    
                    
                    Button(action: {}) {
                        Image("chat-box").resizable()
                            .frame(width: 50, height: 50, alignment: .trailing)
                    }
                    
                }
              
                Divider()
                
                HStack{
                    VStack{
                        
                        Text("Contact Us: (950)7611326")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 220, alignment: .leading)
                    }
                        Image("phone-call").resizable()
                            .frame(width: 30, height: 30, alignment: .center)
                }
                
                Divider()
                
                HStack{
                    VStack{
                        
                        Text("NW Marti Terrace, Miami")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 220, alignment: .leading)
                        Text("FL 33125, United States")
                            .font(.system(size:15))
                            .fontWeight(.medium)
                            .foregroundColor(Color(red: 0.227, green: 0.416, blue: 0.549))
                            .frame(width: 220, alignment: .leading)
                    }
                    
                        Image("placeholder").resizable()
                            .frame(width: 30, height: 30, alignment: .center)
                    
                }
                
                Divider()
                
                
                
               
                
            
            }
            .padding(.bottom, 20.0)
            .frame(width: 300.0,height: 560.0 )
        }
    }
}

struct StoreDetailView_Previews: PreviewProvider {
    static var previews: some View {
        StoreDetailView()
            .previewDevice("iPod touch (7th generation)")
    }
}
