//
//  HomeView.swift
//  togo
//
//  Created by Andrea Montoya on 31/03/21.
//

import Foundation
import SwiftUI

struct HomeView: View {
    var body: some View {
        ZStack {
            LightSlateGrey.ignoresSafeArea()
            VStack {
                
                Text("Welcome, Susan!")
                    .font(.system(size:30))
                    .fontWeight(.medium)
                    .padding()
                    .foregroundColor(.black)
              
             
                HStack {
                    Image("pet").resizable()
                        .frame(width: 80, height: 80, alignment: .center)
                    Button(action: {}) {
                        Text("Find My Pet")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .background(WildBlueYonder)
                            .cornerRadius(5.0)
                    }
                }
                HStack {
                    Image("dog-house").resizable()
                        .frame(width: 80, height: 80, alignment: .center)
                    Button(action: {}) {
                        Text("Services")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .background(WildBlueYonder)
                            .cornerRadius(5.0)
                    }
                }
                HStack {
                    Image("chat-box").resizable()
                        .frame(width: 80, height: 80, alignment: .center)
                    Button(action: {}) {
                        Text("Chat")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .background(WildBlueYonder)
                            .cornerRadius(5.0)
                    }
                }
                
                HStack {
                    Image("user").resizable()
                        .frame(width: 80, height: 80, alignment: .center)
                    Button(action: {}) {
                        Text("My profile")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .background(WildBlueYonder)
                            .cornerRadius(5.0)
                    }
                }
                
                HStack {
                    Image("qr").resizable()
                        .frame(width: 80, height: 80, alignment: .center)
                    Button(action: {}) {
                        Text("QR")
                            .padding()
                            .foregroundColor(Color.white)
                            .frame(width: 150.0)
                            .background(WildBlueYonder)
                            .cornerRadius(5.0)
                    }
                }
                HStack{
                    Text("")
                        .padding()
                        .foregroundColor(Color.white)
                        .frame(width: 150.0)
                        .cornerRadius(5.0)
                    Button(action: {}) {
                        Text("Log out")
                            .padding()
                            .foregroundColor(Color.black)
                            .frame(width: 100.0)
                            .background(LightSlateGrey)
                            .cornerRadius(5.0)
                        Image("log-out").resizable()
                            .frame(width: 25, height: 25, alignment: .center)
                    }
                }
                
            }
            .padding(.bottom, 40)
            .frame(width: 300, height: 570, alignment: .center)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .previewDevice("iPod touch (7th generation)")
    }
}
