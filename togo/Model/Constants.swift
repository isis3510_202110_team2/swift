//
//  Constants.swift
//  togo
//
//  Created by David Manosalva on 30/03/21.
//

import SwiftUI

let QueenBlue:Color = Color(red: 0.227, green: 0.416, blue: 0.549)
let LightSlateGrey:Color = Color(red: 0.423, green: 0.541, blue: 0.616)
let WildBlueYonder:Color = Color(red:0.6, green:0.698, blue:0.866)
let RubyRed:Color = Color(red:0.639, green:0, blue:0.082)
let GrannySmithApple:Color = Color(red:0.705, green:0.898, blue:0.627)
