//
//  User.swift
//  togo
//
//  Created by David Manosalva on 1/04/21.
//

import Foundation

class User: Codable {
    var name: String
    var email: String
    var phoneNumber: String
    var petOwner: Bool
    var pets: Array<Pet>
    var store: Bool
    var vet: Bool
    var walker: Bool
    var imagePath: String
    
    init(name: String, email: String, phone: String) {
        self.email = email
        self.name = name
        self.phoneNumber = phone
        self.imagePath = "https://firebasestorage.googleapis.com/v0/b/proyecto-moviles-31d5c.appspot.com/o/user_pictures%2Fpet_owner%2Fuser.svg?alt=media&token=a31dfcc4-0cbc-4fc2-990a-963c8c1afc37"
        self.petOwner = true
        self.pets = []
        self.store = false
        self.vet = false
        self.walker = false
    }
    
    
    func toDictionary() -> Data? {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        do{
            let dictToReturn = try encoder.encode(self)
            //print(String(data: dictToReturn, encoding: .utf8))
            return dictToReturn
        }
        catch{
            print(error)
        }
        return nil
    }
    
    func fromDictionary(json: Dictionary<String,Any>) -> User {
        var userToReturn = User(name: "User", email: "mail@mail.com", phone: "22222222")
        userToReturn.email = json["email"] as! String
        userToReturn.name = json["name"] as! String
        userToReturn.phoneNumber = json["phoneNumber"] as! String
        userToReturn.imagePath = json["imagePath"] as! String
        userToReturn.petOwner = json["petOwner"]as! Int == 0
        userToReturn.pets = json["pets"]as! Array<Pet>
        userToReturn.store = json["store"]as! Int == 0
        userToReturn.walker = json["walker"]as! Int == 0
        userToReturn.vet = json["vet"]as! Int == 0
        return userToReturn
    }
}
