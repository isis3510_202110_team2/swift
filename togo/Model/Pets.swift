//
//  Pets.swift
//  togo
//
//  Created by David Manosalva on 2/04/21.
//

import Foundation

class Pet : Codable{
    var name: String
    var breed: String
    var height: Int
    var weight: Double
    var birthday: Date
    
    init(name:String, breed:String, height:Int, weight:Double, birthday:Date) {
        self.name = name
        self.breed = breed
        self.height = height
        self.weight = weight
        self.birthday = birthday
    }
    
    func toDictionary() {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        do{
            let dictToReturn = try encoder.encode(self)
            print(String(data: dictToReturn, encoding: .utf8))}
        catch{
            print(error)
        }
    }
    
    func fromDictionary(json: Data) -> Pet? {
        let decoder = JSONDecoder()
        do{
            let objectToReturn = try decoder.decode(Pet.self, from: json)
            return objectToReturn
        }
        catch{
            print(error)
        }
        return nil
    }
}
