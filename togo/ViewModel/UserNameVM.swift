//
//  UserVM.swift
//  togo
//
//  Created by David Manosalva on 2/04/21.
//

import Foundation
import Firebase

internal class UserNameViewModel: ObservableObject
{
    public private(set) var userName:String
    
    init() {
        self.userName = "User"
        self.loadName()
    }
    
    func loadName() {
        self.userName = Auth.auth().currentUser!.displayName ?? "User"
    }
}
