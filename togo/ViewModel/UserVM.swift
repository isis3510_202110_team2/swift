//
//  UserVM.swift
//  togo
//
//  Created by David Manosalva on 2/04/21.
//

import Foundation
import Firebase

internal class UserViewModel: ObservableObject
{
    public private(set) var user:User
    
    init() {
        self.user = User(name: "User", email: "mail@example.com", phone: "+57222222")
        self.loadUser()
    }
    
    func loadUser() {
        let uid = Auth.auth().currentUser!.uid
        let docRef = Firestore.firestore().collection("User").document(uid)

        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                self.user = self.user.fromDictionary(json: document.data()!)
                //print("Document data: \(dataDescription)")
            } else {
                print("Document does not exist")
            }
        }
    }
}
