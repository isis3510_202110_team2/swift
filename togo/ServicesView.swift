//
//  ServicesView.swift
//  togo
//
//  Created by Andrea Montoya on 31/03/21.
//

import Foundation
import SwiftUI

struct ServicesView: View {
    var body: some View {
        ZStack {
            
            
            VStack {
                
                Button(action: {}) {
                    Image("left-arrow").resizable()
                        .frame(width: 25, height: 25, alignment: .leading)
                        .padding()
                        .foregroundColor(Color.black)
                        .frame(width: 100.0)
                        .cornerRadius(5.0)
                }
                
                Text("Services")
                    .font(.system(size:35))
                    .fontWeight(.medium)
                    .padding()
                    .foregroundColor(.black)
                    .frame(alignment: .leading)
                                  
             
                HStack {
                    VStack{
                        Image("veterinary").resizable()
                            .frame(width: 100, height: 100, alignment: .center)
                        Button(action: {}) {
                            Text("Vets")
                                .padding()
                                .foregroundColor(Color.white)
                                .frame(width: 150.0)
                                .background(QueenBlue)
                                .cornerRadius(5.0)
                        }
                    }
                    VStack{
                        Image("store").resizable()
                            .frame(width: 100, height: 100, alignment: .center)
                        Button(action: {}) {
                            Text("Stores")
                                .padding()
                                .foregroundColor(Color.white)
                                .frame(width: 150.0)
                                .background(QueenBlue)
                                .cornerRadius(5.0)
                        }
                    }
                    
                    
                    
                }
                HStack {
                    VStack{
                        
                        Image("dog").resizable()
                            .frame(width: 100, height: 100, alignment: .center)
                        Button(action: {}) {
                            Text("Walkers")
                                .padding()
                                .foregroundColor(Color.white)
                                .frame(width: 150.0)
                                .background(QueenBlue)
                                .cornerRadius(5.0)
                        }
                        
                    }
                    
                    VStack{
                        
                        Image("DayCare").resizable()
                            .frame(width: 100, height: 100, alignment: .center)
                        Button(action: {}) {
                            Text("Day Care")
                                .padding()
                                .foregroundColor(Color.white)
                                .frame(width: 150.0)
                                .background(QueenBlue)
                                .cornerRadius(5.0)
                        }
                        
                    }
                    
                }
            Spacer()
                
               
            }
            .padding(.bottom, 15.0)
        }
    }
}

struct ServicesView_Previews: PreviewProvider {
    static var previews: some View {
        ServicesView()
            .previewDevice("iPod touch (7th generation)")
    }
}
